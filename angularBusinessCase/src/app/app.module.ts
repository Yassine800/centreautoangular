import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdFormComponent } from './ad/ad-form/ad-form.component';
import { AdsListComponent } from './ad/ads-list/ads-list.component';
import { GarageFormComponent } from './garage/garage-form/garage-form.component';
import { GaragesListComponent } from './garage/garages-list/garages-list.component';
import { SingleGarageComponent } from './garage/single-garage/single-garage.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ContactComponent } from './contact/contact.component';
import { ProfileComponent } from './profile/profile.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgxSliderModule} from "@angular-slider/ngx-slider";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from "@angular/common/http";
import { AdDetailComponent } from './ad/ad-detail/ad-detail.component';
import {AdsService} from "./services/ads.service";
import { AdminComponent } from './admin/admin/admin.component';
import { UserComponent } from './user/user/user.component';
import { AdminProfilComponent } from './admin/admin-profil/admin-profil.component';

@NgModule({
  declarations: [
    AppComponent,
    AdFormComponent,
    AdsListComponent,
    GarageFormComponent,
    GaragesListComponent,
    SingleGarageComponent,
    SigninComponent,
    SignupComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SearchBarComponent,
    ContactComponent,
    ProfileComponent,
    AdDetailComponent,
    AdminComponent,
    UserComponent,
    AdminProfilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxSliderModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AdsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
