import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../models/user.model";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user!: User;
  constructor(private http: HttpClient) { }

  ngOnInit(){
    this.getUserInfos();
  }

  getUserInfos() {
    this.http.get('http://localhost:8000/api/show/user').subscribe(
      (data:any) => this.user = data)
    console.log(this.user);
  }

}
