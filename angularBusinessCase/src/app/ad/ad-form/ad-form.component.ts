import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AdsService} from "../../services/ads.service";

@Component({
  selector: 'app-ad-form',
  templateUrl: './ad-form.component.html',
  styleUrls: ['./ad-form.component.css']
})
export class AdFormComponent implements OnInit {
  adForm!: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private adService: AdsService,
              private router: Router) { }

  ngOnInit(): void {
    this.adForm = this.formBuilder.group({
      title: ['', Validators.required],
      garage: ['', Validators.required],
      description: ['', Validators.required],
      releaseYear: ['', Validators.required],
      mileage: ['', Validators.required],
      price: ['', Validators.required],
      brand: ['', Validators.required],
      model: ['', Validators.required],
      fuel: ['', Validators.required],
      photos: ['', Validators.required],
    });
  }

  submitAd(): void {
      this.adService.addAd(this.adForm.value).subscribe(
      )

  }
}
