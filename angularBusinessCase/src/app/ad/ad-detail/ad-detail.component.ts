import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AdsService} from "../../services/ads.service";
import {NUMBER_TYPE} from "@angular/compiler/src/output/output_ast";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-ad-detail',
  templateUrl: './ad-detail.component.html',
  styleUrls: ['./ad-detail.component.css']
})
export class AdDetailComponent implements OnInit {

  ad: any;
  constructor(public route: ActivatedRoute,
              // private router: Router,
              private adService: AdsService,
              ) { }
  // private http: HttpClient
  ngOnInit(): void {

    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.adService.getSingleAd(+id)
    .subscribe(data => {
        this.ad = data;
        console.log(this.ad);
       });
}
}
