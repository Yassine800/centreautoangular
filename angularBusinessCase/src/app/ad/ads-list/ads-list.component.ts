import { Component, OnInit } from '@angular/core';
import {AdsService} from "../../services/ads.service";

@Component({
  selector: 'app-ads-list',
  templateUrl: './ads-list.component.html',
  styleUrls: ['./ads-list.component.css']
})
export class AdsListComponent implements OnInit {
  ads: any = [];

  constructor(private adsService: AdsService) {
  }

  ngOnInit() {
    this.adsService.getAdsList().subscribe(
      (data: any) => this.ads = data)
      // ,setTimeout(()=> console.log(this.ads),
      //   2000)
  }
}




