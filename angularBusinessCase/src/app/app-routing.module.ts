import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AdsListComponent} from "./ad/ads-list/ads-list.component";
import {SigninComponent} from "./auth/signin/signin.component";
import {SignupComponent} from "./auth/signup/signup.component";
import {AdFormComponent} from "./ad/ad-form/ad-form.component";
import {ContactComponent} from "./contact/contact.component";
import {ProfileComponent} from "./profile/profile.component";
import {AdDetailComponent} from "./ad/ad-detail/ad-detail.component";
import {GarageFormComponent} from "./garage/garage-form/garage-form.component";


const routes: Routes = [
  { path: '', redirectTo: 'accueil', pathMatch: 'full' },
  {path: 'accueil', component: HomeComponent},
  {path: 'annonces/:id', component: AdDetailComponent},
  {path: 'connexion', component: SigninComponent},
  {path: 'inscription', component: SignupComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'profil', component: ProfileComponent},
  {path: 'ajout-annonce', component: AdFormComponent},
  {path: 'ajout-garage', component: GarageFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
