export interface User {
  id: number,
  lastname: string,
  firstname: string,
  email: string,
  phoneNumber: string,
  siretNumber: number,
  username: string,
  ads: any,
  garages: any
}
