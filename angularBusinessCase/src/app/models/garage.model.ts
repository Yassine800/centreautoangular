export interface Garage {
    id: number,
    name: string,
    address: string,
    phoneNumber: string
}
