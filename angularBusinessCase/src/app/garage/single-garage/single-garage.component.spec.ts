import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleGarageComponent } from './single-garage.component';

describe('SingleGarageComponent', () => {
  let component: SingleGarageComponent;
  let fixture: ComponentFixture<SingleGarageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleGarageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleGarageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
