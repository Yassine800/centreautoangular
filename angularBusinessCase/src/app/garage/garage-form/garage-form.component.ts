import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Garage} from "../../models/garage.model";
import {GaragesService} from "../../services/garages.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-garage-form',
  templateUrl: './garage-form.component.html',
  styleUrls: ['./garage-form.component.css']
})
export class GarageFormComponent implements OnInit {
  addForm!: FormGroup;

  garage: Garage[] | undefined;
  // formValue = this.addForm.value;

  constructor(private formBuilder: FormBuilder,
              private garageService: GaragesService,
              private router: Router ) { }

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      phoneNumber: ['', Validators.required],
    });
  }

  addOne() {
    this.garageService.addGarage(this.addForm.value).subscribe(
    )
      this.addForm.reset();
      this.router.navigate(['/accueil']);

  }
}


