import { Component, OnInit } from '@angular/core';
import {LabelType, Options} from '@angular-slider/ngx-slider';
import {AdsService} from "../services/ads.service";
import {Observable} from "rxjs";
import {Brand} from "../models/brand.model";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  ads: any = [];
  brands: any = [];
  fuels: any = [];
  models: any = [];
  brandName = 'Marque';
  modelName = 'Modèle';
  fuelName = 'Carburant';
  brandId = 0;
  modelId = 0;
  fuelId = 0;
  constructor(private adsService: AdsService,
              private http: HttpClient,
              private router: Router ) {
  }

  ngOnInit(): void {
    this.showAllBrands()
    this.showAllFuels()
  }

  getBrands(): any {
    return this.http.get<any>('http://localhost:8000/brands/');
  }

  showAllBrands() {
    this.getBrands().subscribe(
      (data: any) => {
        this.brands = data }
      )
  }

  getModels(id: number): any {
    return this.http.get<any>('http://localhost:8000/brand/models/' +id).subscribe(
      (data:any) => {
        this.models = data
      })
  }

  showAllFuels() {
    this.adsService.getFuelsList().subscribe(
      (data: any) => {
        this.fuels = data }
    )
  }

  switchBrandName(id: number, val: string) {
    this.brandName = val;
    this.brandId = id;
    this.adsService.getBrandId(id);
    this.getModels(this.brandId);
  }

  switchModelName(id: number, val: string): void{
    this.modelName = val;
    this.modelId = id;
    this.adsService.getModelId(id);
  }

  switchFuelName(id: number, val: string): void{
    this.fuelName = val;
    this.fuelId = id;
    this.adsService.getFuelId(id);
  }

  searchByFilters() {
    this.adsService.customFilter()
  }

  minValue = 1980;
  maxValue = 2021;
  options: Options = {
    floor: 1980,
    ceil: 2021,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
        case LabelType.High:
        default:
          return '' + value;
      }
    }
  };

  minValue2 = 0;
  maxValue2 = 200000;
  options2: Options = {
    floor: 0,
    ceil: 200000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
        case LabelType.High:
        default:
          return '' + value;
      }
    }
  };

  minValue3 = 0;
  maxValue3 = 30000;
  options3: Options = {
    floor: 0,
    ceil: 30000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
        case LabelType.High:
        default:
          return '€' + value;
      }
    }
  };
}

// getBrandId(id: number) {
//   this.brandId = id;
// }
//
// getModelId(id: number) {
//   this.modelId = id;
// }
//
//   getFuelId(id: number) {
//     this.fuelId = id;
//   }




// this.adsService.getAdsList().subscribe(
//   (data: any) => {this.ads = data;
//     this.brandsTable = this.ads.map(
//       element => element.brand)
//     console.log(this.brandsTable);
//   });
// showModels(id: number){
//   this.getModels(id).subscribe(
//     (data:any) => {
//       this.models = data,
//     console.log(this.models) }
//   )
// }

// onChangeBrand(id: number){
//   this.getModels(+id).subscribe(
//     (data:any) => {
//       this.models = data
//
//     }
//   );
// }
