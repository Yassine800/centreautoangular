import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import jwtDecode from "jwt-decode";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  signInForm!: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.signInForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  // getForm() {
  //   return this.signInForm.controls;
  // }

  onSubmit() {
    if (this.signInForm.valid) {
      this.authService.login(this.signInForm.value)
        .subscribe(
          (data: any) => {

            this.authService.changeLogStatus();
            this.authService.setToken(data.token);
            const token: any = jwtDecode(data.token);
            this.authService.setUsername(token.username);
            this.authService.checkRole(token.roles)
            if(this.authService.ifAdmin()){
              this.router.navigate(['admin/profil']);
            }
            else {
              this.router.navigate(['/profil']);
            }
          }
        );
    }
  }
}
