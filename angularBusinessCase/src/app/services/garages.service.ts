import { Injectable } from '@angular/core';
import {Garage} from "../models/garage.model";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GaragesService {
  url = 'http://localhost:8000/create/garage';
  constructor(private http: HttpClient) { }

  addGarage(garage: Garage): Observable<any> {
    return this.http.post<any>(this.url, garage);
  }


}


