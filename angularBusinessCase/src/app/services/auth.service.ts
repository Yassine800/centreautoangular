import { Injectable } from '@angular/core';
import {HttpClient } from "@angular/common/http";

export interface User {
  id?: number,
  name?: string,
  password?: string,
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn = false;
  statusAdmin: boolean = false;
  constructor(private http: HttpClient) { }

    login(user: User) {
      return this.http.post('http://localhost:8000/api/login_check', user);
  }

    getLogStatus() {
      return this.isLoggedIn;
    }

    changeLogStatus() {
      this.isLoggedIn = !this.isLoggedIn;
    }

    setToken(data: any) {
      sessionStorage.setItem("token", data);
    }

    getToken(): any {
      return sessionStorage.getItem("token");
    }

    setUsername(data: any) {
      sessionStorage.setItem("username", data);
    }

    getUsername(): any {
      return sessionStorage.getItem("username");
    }

    checkRole(data: any) {
    if(data.length == 2) {
      sessionStorage.setItem("admin", "true");
       }
    }

    ifAdmin(): any {
      return sessionStorage.getItem("admin")
    }

    isAdmin(): any {
      return this.statusAdmin;
    }

}
