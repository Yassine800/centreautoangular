import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Brand} from "../models/brand.model";
import {Router} from "@angular/router";



@Injectable({
  providedIn: 'root'
})
export class AdsService {
  private apiUrl = "http://localhost:8000/ads";
  brandId = 0;
  modelId = 0;
  fuelId = 0;
  constructor(private http: HttpClient,
              private router: Router) {
  }


  getBrandId(id: number) {
    this.brandId = id;
  }
  getModelId(id: number) {
    this.modelId = id;
  }
  getFuelId(id: number) {
    this.fuelId = id;
  }

  // Méthode pour récupérer une annonce
  getSingleAd(id: number): Observable<any> {
    return this.http.get<any>('http://localhost:8000/api/show/ad/' + id);
  }

  //Méthode pour récupérer toutes les annonces
  getAdsList(): any {
    return this.http.get(this.apiUrl);
  }

  // Méthode pour l'ajout d'une annonce
  addAd(ad: any): Observable<any> {
    return this.http.post('http://localhost:8000/create/ad', ad);
  }

  // Méthode pour récupérer la liste des carburants
  getFuelsList(): any {
    return this.http.get('http://localhost:8000/fuels');
  }

  customFilter() {
    this.searchAds();
  }
  searchAds() {
    this.http.post('http://localhost:8000/filter/ads', {
        brand: this.brandId,
        model: this.modelId,
        fuel: this.fuelId
      }
    ).subscribe(data => {
      console.log(data);
    })
  }
}
